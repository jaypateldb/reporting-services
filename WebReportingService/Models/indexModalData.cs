﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebReportingService.Models
{
    public class indexModalData
    {
        public string betpl { get; set; }
        public string betvolume { get; set; }
        public string avgplayers { get; set; }
        public string noofbets { get; set; }
        public string Lastweekpl { get; set; }
        public string lastmonthpl { get; set; }

        private DateTime _SetfromDate =Convert.ToDateTime( DateTime.Now.ToString("yyyy-MM-dd"));
        private DateTime _SettoDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));

        public DateTime todate
        {
            get
            {
                return _SettoDate;
            }
            set
            {

                _SettoDate = value;
            }
        }
        public DateTime fromdate
        {
            get
            {
                return _SetfromDate;
            }
            set
            {
                _SetfromDate = value;
            }
        }
        public List<SelectListItem> Games { get; set; }
        public int[] gameid { get; set; }
        public List<SelectListItem> Platforms { get; set; }
        public int[] platformids { get; set; }
        
    }
}
