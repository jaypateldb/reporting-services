﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebReportingService.Models
{
    public class ChangePasswordModalData
    {
        [Key]
        public int? userid { get; set; }

        [Display(Name = "Enter Username")]
        public string username { get; set; }


        [Display(Name = "Enter Old Password")]
        [Required(ErrorMessage = "Please Enter Old Password")]
        [DataType(DataType.Password)]
        public string useroldpassword { get; set; }


        [Display(Name = "Enter New Password")]
        [Required(ErrorMessage = "Please New Password")]
        [DataType(DataType.Password)]
        public string userpassword { get; set; }


        [Display(Name = "Enter Confirm Password")]
        [Required(ErrorMessage = "Please Enter Confirm Password")]
        [DataType(DataType.Password)]
        public string userconpassword { get; set; }

    }
}
