﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebReportingService.Models
{
    public class LoginModalData
    {
        [Key]
        public int? userid { get; set; }

        [Display(Name ="Enter Username")]
        [Required(ErrorMessage ="Please Enter Username")]
        public string username { get; set; }


        [Display(Name = "Enter Userpassword")]
        [Required(ErrorMessage = "Please Enter Userpassword")]
        [DataType(DataType.Password)]
        public string userpassword { get; set; }

        public bool rememberchr { get; set; }

    }
}
