﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using WebReportingService.Models;

namespace WebReportingService.Controllers
{
    public class HomeController : Controller
    {
        //SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-LV0L0J3\SQLEXPRESS2019;Initial Catalog=dev_casino;Persist Security Info=True;User ID=sa;Password=admin@12345");
        //public static SqlConnection con = new SqlConnection(@"Data Source=81.19.210.250;Initial Catalog=dev_casino;Persist Security Info=True;User ID=dev_user2;Password=Dev@123");
        public static SqlConnection con = new SqlConnection(@"Data Source=51.89.183.56,1533;Initial Catalog=ReportingService;Persist Security Info=True;User ID=rps;Password=admin@12345");

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }


        public IActionResult Index(indexModalData id)
        {
            if (HttpContext.Session.GetString("userid") == null || HttpContext.Session.GetString("userid") == "")
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                try
                {
                    string gameid = "0", platformid = "0", platformname = "0";
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    id.Platforms = FillPlatform();
                    if (id.platformids != null)
                    {
                        List<SelectListItem> selectedItems = id.Platforms.Where(p => id.platformids.Contains(int.Parse(p.Value))).ToList();
                        platformname = "";
                        foreach (var selectedItemp in selectedItems)
                        {
                            selectedItemp.Selected = true;

                            platformid = selectedItemp.Value.ToString();
                            if (platformid == "0" || platformid == null || platformid == "")
                            {
                                platformname = "[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]";
                                break;
                            }
                            else if (platformid == "1")
                            {
                                if (platformname != "")
                                {
                                    platformname = platformname + "," + "[LNK_SuperNowa].[dev_casino].[dbo]";
                                }
                                else
                                {
                                    platformname = "[LNK_SuperNowa].[dev_casino].[dbo]";
                                }
                            }
                            else if (platformid == "2")
                            {
                                if (platformname != "")
                                {
                                    platformname = platformname + "," + "[LNK_SuperNowa].[dev_virtual_casino].[dbo]";
                                }
                                else
                                {
                                    platformname = "[LNK_SuperNowa].[dev_virtual_casino].[dbo]";
                                }
                            }
                            else if (platformid == "3")
                            {
                                if (platformname != "")
                                {
                                    platformname = platformname + "," + "LNK_MYSQL";
                                }
                                else
                                {
                                    platformname = "LNK_MYSQL";
                                }
                            }


                        }
                    }
                    else
                    {
                        platformname = "[LNK_SuperNowa].[dev_casino].[dbo],[LNK_SuperNowa].[dev_virtual_casino].[dbo]";
                    }

                    id.Games = FillGames();
                    if (id.gameid != null)
                    {
                        //selectedItem.Selected = true;
                        //gameid = selectedItem.Value.ToString();
                        List<SelectListItem> selectedItems = id.Games.Where(p => id.gameid.Contains(int.Parse(p.Value))).ToList();
                        gameid = "";
                        foreach (var selectedItemp in selectedItems)
                        {
                            selectedItemp.Selected = true;
                            //gameid = selectedItemp.Value.ToString();
                            if ( gameid != "")
                            {
                                gameid = gameid + "," + selectedItemp.Value.ToString();
                            }
                            else
                            {
                                gameid = selectedItemp.Value.ToString();
                            }
                        }
                    }
                    else
                    {
                        gameid = "0";
                    }



                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_getreportdata";
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@platformname", platformname);
                    cmd.Parameters.AddWithValue("@gameid", gameid);
                    cmd.Parameters.AddWithValue("@fromdate", id.fromdate);
                    cmd.Parameters.AddWithValue("@todate", id.todate);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    SqlDataReader dr;
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        ViewBag.betpl = dr["BETPL"].ToString();
                        ViewBag.betvolume = dr["VOLUME"].ToString();
                        ViewBag.avgplayers = dr["#PLAYER"].ToString();
                        ViewBag.noofbets = dr["#BET"].ToString();

                        ViewBag.fromdate = id.fromdate.ToString("dd MMM, yyyy");
                        ViewBag.todate = id.todate.ToString("dd MMM, yyyy");
                        ViewBag.AVG_VOL_BET = dr["AVG_VOL_BET"].ToString();
                        ViewBag.AVG_VOL_PLAYER = dr["AVG_VOL_PLAYER"].ToString();
                        dr.Close();
                        cmd.Dispose();

                        return View(id);
                    }
                    else
                    {
                        ViewBag.betpl = "0";
                        ViewBag.betvolume = "0";
                        ViewBag.avgplayers = "0";
                        ViewBag.noofbets = "0";
                        ViewBag.fromdate = DateTime.Now.ToString("dd MMM, yyyy");
                        ViewBag.todate = DateTime.Now.ToString("dd MMM, yyyy");
                        ViewBag.AVG_VOL_BET = "0";
                        ViewBag.AVG_VOL_PLAYER = "0";
                        dr.Close();
                        cmd.Dispose();
                        return View(id);
                    }
                }
                catch (Exception er)
                {
                    ViewData["Message"] = er.ToString();
                    return View(id);
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
        }
        public static List<SelectListItem> FillPlatform()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_GetPlatformList";
            cmd.Connection = con;
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                while (sdr.Read())
                {
                    items.Add(new SelectListItem
                    {
                        Text = sdr["PlatName"].ToString(),
                        Value = sdr["ID"].ToString()
                    });
                }
            }


            return items;
        }
        public static List<SelectListItem> FillGames()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_GetGameList";
            cmd.Connection = con;
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                while (sdr.Read())
                {
                    items.Add(new SelectListItem
                    {
                        Text = sdr["GameName"].ToString(),
                        Value = sdr["GameID"].ToString()
                    });
                }
            }


            return items;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "B145D2A9EDB6FAB7AF7757B1913A5";
            byte[] clearBytes = Encoding.UTF8.GetBytes(clearText);
            byte[] clearBytes1;
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                    clearBytes1 = Encoding.UTF8.GetBytes(clearText);
                }
            }
            return Convert.ToBase64String(clearBytes1);
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "B145D2A9EDB6FAB7AF7757B1913A5";
            cipherText = cipherText.Replace(" ", "+");
            var base64EncodedBytes = System.Convert.FromBase64String(cipherText);
            string clearstring = Encoding.UTF8.GetString(base64EncodedBytes);

            cipherText = clearstring;

            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.UTF8.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            ChangePasswordModalData cp = new ChangePasswordModalData();
            cp.username = HttpContext.Session.GetString("username");

            cp = new ChangePasswordModalData();
            cp.userconpassword = "";
            //cp.userid = Convert.ToInt32(HttpContext.Session.GetString("userid"));
            cp.userid = HttpContext.Session.GetInt32("userid");
            cp.username = HttpContext.Session.GetString("username");
            cp.useroldpassword = "";
            cp.userpassword = "";
            return View(cp);
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordModalData cp)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                if (cp.userpassword == cp.userconpassword)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_changepassword";
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@userid", Convert.ToInt32(HttpContext.Session.GetString("userid")));
                    cmd.Parameters.AddWithValue("@useroldpwd", Encrypt(cp.useroldpassword));
                    cmd.Parameters.AddWithValue("@userpassword", Encrypt(cp.userpassword));
                    cmd.Parameters.Add("@status", SqlDbType.Int);
                    cmd.Parameters["@status"].Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    int retVal = Convert.ToInt32(cmd.Parameters["@status"].Value);
                    if (retVal == 1)
                    {
                        cp = new ChangePasswordModalData();
                        cp.username = HttpContext.Session.GetString("username");
                        ViewData["Message"] = "Password Changed";
                        return View(cp);
                    }
                    else
                    {
                        cp = new ChangePasswordModalData();
                        cp.username = HttpContext.Session.GetString("username");
                        ViewData["Message"] = "Please Check Old Password";
                        return View(cp);
                    }

                }
                else
                {
                    ViewData["Message"] = "New Password and Old Password Can not be match";
                    return View(cp);
                }

            }
            catch (Exception er)
            {
                ViewData["Message"] = er.ToString();
                return View(cp);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        [HttpGet]
        public IActionResult Signinout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Login");
        }
    }
}
