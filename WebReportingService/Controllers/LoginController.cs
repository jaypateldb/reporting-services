﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WebReportingService.Models;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace WebReportingService.Controllers
{
    public class LoginController : Controller
    {
        //SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-LV0L0J3\SQLEXPRESS2019;Initial Catalog=ReportingService;Persist Security Info=True;User ID=sa;Password=admin@12345");
        public static SqlConnection con = new SqlConnection(@"Data Source=51.89.183.56,1533;Initial Catalog=ReportingService;Persist Security Info=True;User ID=rps;Password=admin@12345");

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(LoginModalData lc)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                if (lc.username != null && lc.userpassword != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_login";
                    cmd.Parameters.AddWithValue("@username", Encrypt(lc.username));
                    cmd.Parameters.AddWithValue("@userpassword", Encrypt(lc.userpassword));
                    cmd.Connection = con;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        HttpContext.Session.SetString("userid", dr["userid"].ToString());
                        HttpContext.Session.SetString("username", Decrypt(dr["username"].ToString()));
                        ViewData["Message"] = HttpContext.Session.GetString("userid");
                        dr.Close();
                    }
                    else
                    {
                        dr.Close();
                    }

                    if (ViewData["Message"] != null)
                    {
                        if (lc.rememberchr == true)
                        {
                            //HttpContext.Request.Cookies
                        }
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewData["Message"] = "Invalid Credential";
                        return View(lc);
                    }
                    //string sp_qry = "select * from login where username=@username and userpassword=@userpassword";
                    //SqlCommand cmd = new SqlCommand(sp_qry, con);
                    //cmd.Parameters.AddWithValue("@username", Encrypt(lc.username));
                    //cmd.Parameters.AddWithValue("@userpassword", Encrypt(lc.userpassword));
                    //SqlDataReader dr = cmd.ExecuteReader();
                    //if (dr.HasRows)
                    //{
                    //    dr.Read();
                    //    HttpContext.Session.SetString("userid", dr["userid"].ToString());
                    //    HttpContext.Session.SetString("username", Decrypt(dr["username"].ToString()));
                    //    ViewData["Message"] = HttpContext.Session.GetString("userid");
                    //    dr.Close();
                    //}
                    //else
                    //{
                    //    dr.Close();
                    //}

                    //if (ViewData["Message"] != null)
                    //{
                    //    if (lc.rememberchr == true)
                    //    {
                    //        //HttpContext.Request.Cookies
                    //    }
                    //    return RedirectToAction("Index", "Home");
                    //}
                    //else
                    //{
                    //    ViewData["Message"] = "Invalid User";
                    //    return View(lc);
                    //}
                }
                else
                {

                    return View();
                }

            }
            catch (Exception er)
            {
                ViewData["Message"] = er.ToString();
                return View(lc);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "B145D2A9EDB6FAB7AF7757B1913A5";
            byte[] clearBytes = Encoding.UTF8.GetBytes(clearText);
            byte[] clearBytes1;
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                    clearBytes1 = Encoding.UTF8.GetBytes(clearText);
                }
            }
            return Convert.ToBase64String(clearBytes1);
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "B145D2A9EDB6FAB7AF7757B1913A5";

            cipherText = cipherText.Replace(" ", "+");
            var base64EncodedBytes = System.Convert.FromBase64String(cipherText);
            string clearstring = Encoding.UTF8.GetString(base64EncodedBytes);
            cipherText = clearstring;

            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.UTF8.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

    }
}
